
Model info for convnext_nano.in12k_ft_in1k
==========================================


Sequential (Input shape: 64 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     64 x 80 x 56 x 56   
Conv2d                                    3920       True      
LayerNorm2d                               160        True      
Identity                                                       
Conv2d                                    4000       True      
LayerNorm2d                               160        True      
____________________________________________________________________________
                     64 x 320 x 56 x 56  
Conv2d                                    25920      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 80 x 56 x 56   
Conv2d                                    25680      True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    4000       True      
LayerNorm2d                               160        True      
____________________________________________________________________________
                     64 x 320 x 56 x 56  
Conv2d                                    25920      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 80 x 56 x 56   
Conv2d                                    25680      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm2d                               160        True      
____________________________________________________________________________
                     64 x 160 x 28 x 28  
Conv2d                                    51360      True      
Conv2d                                    8000       True      
LayerNorm2d                               320        True      
____________________________________________________________________________
                     64 x 640 x 28 x 28  
Conv2d                                    103040     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 160 x 28 x 28  
Conv2d                                    102560     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    8000       True      
LayerNorm2d                               320        True      
____________________________________________________________________________
                     64 x 640 x 28 x 28  
Conv2d                                    103040     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 160 x 28 x 28  
Conv2d                                    102560     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm2d                               320        True      
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    205120     True      
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    16000      True      
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 1280 x 14 x 14 
Conv2d                                    410880     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 320 x 14 x 14  
Conv2d                                    409920     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm2d                               640        True      
____________________________________________________________________________
                     64 x 640 x 7 x 7    
Conv2d                                    819840     True      
Conv2d                                    32000      True      
LayerNorm2d                               1280       True      
____________________________________________________________________________
                     64 x 2560 x 7 x 7   
Conv2d                                    1640960    True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 640 x 7 x 7    
Conv2d                                    1639040    True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    32000      True      
LayerNorm2d                               1280       True      
____________________________________________________________________________
                     64 x 2560 x 7 x 7   
Conv2d                                    1640960    True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 640 x 7 x 7    
Conv2d                                    1639040    True      
Dropout                                                        
Identity                                                       
Identity                                                       
Identity                                                       
____________________________________________________________________________
                     64 x 640 x 1 x 1    
AdaptiveAvgPool2d                                              
AdaptiveMaxPool2d                                              
____________________________________________________________________________
                     64 x 1280           
Flatten                                                        
BatchNorm1d                               2560       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 512            
Linear                                    655360     True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 12             
Linear                                    6144       True      
____________________________________________________________________________

Total params: 15,612,048
Total trainable params: 15,612,048
Total non-trainable params: 0

Optimizer used: <function Adam at 0x7f9009d7be20>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback